package com.uennar.hockeymanager;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;


import com.uennar.hockeymanager.data.DbHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


import com.uennar.hockeymanager.data.Game;
import com.uennar.hockeymanager.data.Player;
import com.uennar.hockeymanager.data.Team;
import com.uennar.hockeymanager.data.TeamList;
import com.uennar.hockeymanager.spinner.TeamSpinnerAdapter;

public class MainActivity extends AppCompatActivity {

    private Resources resources;
    private Spinner spinnerA;
    private Spinner spinnerB;
    private SpinnerAdapter teamSpinnerAdapter;
    private TextView teamATV;
    private TextView teamBTV;
    private TextView teamAScoreTV;
    private TextView teamBScoreTV;
    private TextView teamAPowerTV;
    private TextView teamBPowerTV;
    private TextView teamAGoalTV;
    private TextView teamBGoalTV;
    private ImageView goalAnimationView;
    private TextView periodTV;
    private Button playBtn;
    private Button statBtn;
    private Button playersBtn;
    private ProgressBar timeBar;
    private String teamAScore;
    private String teamBScore;
    private int teamAPower;
    private int teamBPower;
    private Handler timeBarHandler;
    private Handler resultHandler;
    private Handler periodHandler;
    private Handler scoreAHandler;
    private Handler scoreBHandler;
    private Handler goalHandler;

    private DbHelper dbHelper;

    private List<Team> teamList;
    private Team teamA;
    private Team teamB;

    private LinearLayout arena;
    private LinearLayout teamAGoals;
    private LinearLayout teamBGoals;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);
        initUIElements();
        resources = getResources();

        teamList = new TeamList(resources, getPackageName(), this).initTeamList();

        teamSpinnerAdapter = new TeamSpinnerAdapter(this,
                R.layout.team_spinner_layout, R.id.team_name, teamList);

        addTeamsOnSpinner(spinnerA);
        addTeamsOnSpinner(spinnerB);

        dbHelper = new DbHelper(this);


        scoreAHandler = new Handler(){
            public void handleMessage(Message msg) {
                if (msg.what == 111) {
                    teamAGoalTV.setText("");
                    return;
                }

                List<Player> players = teamA.getPlayers();
                int num = new Random().nextInt(players.size());

                Bundle bundle = msg.getData();
                String scoreTime = bundle.getString("score");
                String[] strArr = scoreTime.split("-");

                String score = strArr[0];
                if (!score.equals(teamAScore)) {
                    String playerName = players.get(num).getName();
                    String txt = playerName + " kicks goal";
                    setTVText(teamAGoalTV, txt);
                    addGoalsTV(teamAGoals, strArr[1] + " " + playerName);
                }

                teamAScoreTV.setText(score);


            }
        };
        scoreBHandler = new Handler(){
            public void handleMessage(Message msg) {
                if (msg.what == 111) {
                    teamBGoalTV.setText("");
                    return;
                }
                List<Player> players = teamB.getPlayers();
                int num = new Random().nextInt(players.size());
                Bundle bundle = msg.getData();
                String scoreTime = bundle.getString("score");
                String[] strArr = scoreTime.split("-");

                String score = strArr[0];
                if (!score.equals(teamBScore)) {
                    String playerName = players.get(num).getName();
                    String txt = playerName + " kicks goal";
                    setTVText(teamBGoalTV, txt);
                    addGoalsTV(teamBGoals, strArr[1] + " " + playerName);
                }

                teamBScoreTV.setText(score);

            }
        };

        periodHandler = new Handler(){
            public void handleMessage(Message msg) {
                periodTV.setText(String.valueOf(msg.what));

            }
        };

        timeBarHandler = new Handler() {
            public void handleMessage(Message msg) {
                timeBar.setProgress(msg.what);
            }
        };

        resultHandler = new Handler() {
            public void handleMessage(Message msg) {
                Bundle bundle = msg.getData();
                String result = bundle.getString("result");
                String[] strArr = result.split("-");
                teamAScore = strArr[0];
                teamBScore = strArr[2];
                setResult(strArr[1]);
            }
        };

        goalHandler = new Handler() {

            public void handleMessage(Message msg) {

                if (msg.what == 1 ) {
                    goalAnimationView.setVisibility(View.VISIBLE);
                }
                else {
                    goalAnimationView.setVisibility(View.INVISIBLE);
                }
            }
        };




    }

    private void initUIElements() {


        spinnerA = (Spinner) findViewById(R.id.teamASpinner);
        spinnerB = (Spinner) findViewById(R.id.teamBSpinner);

        teamATV = (TextView) findViewById(R.id.teamA);
        teamBTV = (TextView) findViewById(R.id.teamB);

        teamAScoreTV = (TextView) findViewById(R.id.teamAScore);
        teamBScoreTV = (TextView) findViewById(R.id.teamBScore);

        teamAPowerTV = (TextView) findViewById(R.id.teamAPower);
        teamBPowerTV = (TextView) findViewById(R.id.teamBPower);

        teamAGoalTV = findViewById(R.id.teamAGoal);
        teamBGoalTV = findViewById(R.id.teamBGoal);

        timeBar = findViewById(R.id.timeBar);

        playBtn = findViewById(R.id.playBtn);
        statBtn = findViewById(R.id.statBtn);
        playersBtn = findViewById(R.id.playersBtn);

        arena = findViewById(R.id.arena);
        teamAGoals = findViewById(R.id.teamAGoals);
        teamBGoals = findViewById(R.id.teamBGoals);



        periodTV = findViewById(R.id.period);

        goalAnimationView = findViewById(R.id.goalAnimation);


    }

    private void addTeamsOnSpinner(Spinner teamSpinner) {


        teamSpinner.setAdapter(teamSpinnerAdapter);
        // заголовок
        teamSpinner.setPrompt("Teams");


        teamSpinner.setSelection(0);


        teamSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                teamAScoreTV.setText("0");
                teamBScoreTV.setText("0");
                teamAGoals.removeAllViews();
                teamBGoals.removeAllViews();
                if (position != 0) {

                    if (parent.getId() == spinnerA.getId()) {
                        teamA = ((Team) parent.getItemAtPosition(position));
                        //setTVText(teamATV, teamA.getName());
                        teamAPower = teamA.getPower();
                        //setTVText(teamAPowerTV, "Power " + String.valueOf(teamAPower));
                        arena.setBackgroundResource(teamA.getArenaId());
                    }
                    if (parent.getId() == spinnerB.getId()) {
                        teamB = ((Team) parent.getItemAtPosition(position));
                        //setTVText(teamBTV, teamB.getName());
                        teamBPower = teamB.getPower();
                        //setTVText(teamBPowerTV, "Power " + String.valueOf(teamBPower));
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }


    public void play(View view) {
        teamAScoreTV.setText("0");
        teamBScoreTV.setText("0");
        teamAGoals.removeAllViews();
        teamBGoals.removeAllViews();
        if (spinnerA.getSelectedItemPosition() != 0 & spinnerB.getSelectedItemPosition() != 0) {

            if (teamA.equals(teamB)) {
                Toast.makeText(getBaseContext(), "Please, select different teams", Toast.LENGTH_SHORT).show();
            } else {
                setEnabled(false, playBtn, statBtn, playersBtn, spinnerA, spinnerB);

                timeBar.setVisibility(ProgressBar.VISIBLE);



                Game game = new Game(teamA, teamB, timeBarHandler, resultHandler,
                        periodHandler, goalHandler, scoreAHandler, scoreBHandler, dbHelper);

                game.play2();


            }
        } else {
            Toast.makeText(getBaseContext(), "Select both teams", Toast.LENGTH_SHORT).show();
        }
    }

    public void setTVText(TextView textView, String txt) {
        textView.setText(String.valueOf(txt));
    }

    public void toStat(View view) {
        Intent intent = new Intent(MainActivity.this, StatActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void toPlayers(View view) {
        Intent intent = new Intent(MainActivity.this, PlayerTableActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void setResult(String result) {
        Toast.makeText(getBaseContext(), result, Toast.LENGTH_SHORT).show();
        setTVText(teamAScoreTV, String.valueOf(teamAScore));
        setTVText(teamBScoreTV, String.valueOf(teamBScore));
        setEnabled(true, playBtn, statBtn, playersBtn, spinnerA, spinnerB);
        timeBar.setVisibility(ProgressBar.INVISIBLE);
        timeBar.setProgress(0);
    }

    private void setEnabled(boolean isEnabled, View... views) {
        for (View view : views
                ) {
            view.setEnabled(isEnabled);
        }

    }
    private void addGoalsTV(LinearLayout layout, String txt) {
        TextView tv = new TextView(this);
        tv.setText(txt);
        layout.addView(tv);
    }
}
