package com.uennar.hockeymanager;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


import com.uennar.hockeymanager.data.DbHelper;
import com.uennar.hockeymanager.data.Query;

import java.util.ArrayList;
import java.util.List;

import static com.uennar.hockeymanager.data.Contract.Stat;

/**
 * Created by Halim on 20.04.2018.
 */

public class StatActivity extends Activity {
    private int count;

    private DbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_stat);


        dbHelper = new DbHelper(this);

        displayStatTable();

    }

    public void toMainActivity(View view) {
        Intent intent = new Intent(StatActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }


    private void displayStatTable() {

        // Создадим и откроем для чтения базу данных
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String [] projection = new String[]{
                Stat.NAME_COLUMN, Stat.WON_COLUMN,
                Stat.DRAW_COLUMN, Stat.LOST_COLUMN}; //массив столбцов

        Query query = new Query(db, Stat.TABLE_NAME, projection);

        try {
            List<String> texts = new ArrayList<>();
            texts.add("");
            texts.add("W");
            texts.add("D");
            texts.add("L");
            createTableRows(texts);
            List<List<String>> rowList = query.getRowsList(null, null);
            for (List<String> cellValueList: rowList
                    ) {
                createTableRows(cellValueList);

            }

        } catch (Exception e) {
            Log.e("testException", e.getMessage());
        }


    }

    private void createTableRows(List<String> texts) {

        TableLayout tableStatLayout = findViewById(R.id.tableStatLayout);
        TableRow row = new TableRow(this);
        if (count == 0) {
            row.setBackgroundColor(Color.GRAY);
            count = 1;
        }

        for (String str : texts) {
            TextView textView = new TextView(this);
            //textView.setTextColor(Color.CYAN);
            textView.setText(str);
            row.addView(textView);
        }
        tableStatLayout.addView(row);
    }
}
