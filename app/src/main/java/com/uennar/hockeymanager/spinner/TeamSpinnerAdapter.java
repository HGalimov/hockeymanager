package com.uennar.hockeymanager.spinner;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.uennar.hockeymanager.R;
import com.uennar.hockeymanager.data.Team;


import java.util.List;

public class TeamSpinnerAdapter extends ArrayAdapter<Team> {
    int groupid;

    List<Team> teamList;
    LayoutInflater inflater;

    public TeamSpinnerAdapter(Activity context, int groupid, int id, List<Team>
            teamList) {
        super(context, id, teamList);
        this.teamList = teamList;
        this.teamList.add(0, new Team("Select team"));
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupid = groupid;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View itemView = inflater.inflate(groupid, parent, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.team_img);
        if (position != 0) {
            imageView.setImageResource(teamList.get(position).getImageId());
        }
        TextView textView = (TextView) itemView.findViewById(R.id.team_name);
        textView.setText(teamList.get(position).getName());
        return itemView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup
            parent) {
        return getView(position, convertView, parent);

    }
}