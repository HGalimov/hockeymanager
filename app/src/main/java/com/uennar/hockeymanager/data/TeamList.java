package com.uennar.hockeymanager.data;

import android.content.Context;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.uennar.hockeymanager.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Halim on 25.08.2018.
 */

public class TeamList {
    private String[] teamsArray;
    private int[] powersArray;
    private String[] logoArray;
    private String[] arenaArray;
    private Resources resources;
    private String packageName;
    private Context context;

    public TeamList(Resources resources, String packageName, Context context) {
        this.resources = resources;
        this.packageName = packageName;
        this.context = context;
    }

    public List<Team> initTeamList(){
        List<Team> teamList = new ArrayList<>();
        //получение массива строк команд из ресурса
        teamsArray = resources.getStringArray(R.array.teams_array);

        logoArray = resources.getStringArray(R.array.teams_logo_array);

        arenaArray = resources.getStringArray(R.array.teams_arena_array);

        //получение массива строк мощности команд из ресурса
        powersArray = resources.getIntArray(R.array.teams_power_array);
        for (int i = 0; i < teamsArray.length; i++) {
            int logoId = getDrawId(logoArray[i]);
            int arenaId = getDrawId(arenaArray[i]);
            Team team = new Team(teamsArray[i], logoId, arenaId, powersArray[i]);
            List<Player> playersList = null;
            try {
                playersList = new PlayerList(context, team).getPlayerList();
            } catch (Exception e) {
                e.printStackTrace();
            }
            team.setPlayers(playersList);
            teamList.add(team);
        }
        return teamList;
    }
    private int getDrawId(String item) {
        return resources.getIdentifier(item, "drawable", packageName);
    }
}
