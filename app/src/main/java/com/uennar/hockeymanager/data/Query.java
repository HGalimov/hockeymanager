package com.uennar.hockeymanager.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.uennar.hockeymanager.data.Contract.Stat;

import java.util.ArrayList;
import java.util.List;


public class Query {
    private SQLiteDatabase db;
    private String[] projection;
    private String tableName;

    public Query(SQLiteDatabase db, String tableName, String[] projection) {
        this.db = db;
        this.tableName = tableName;
        this.projection = projection;
    }

    public Query(SQLiteDatabase db) {
        this.db = db;
    }

    public void updStatTable(String colName, String team) { //пока только для сохранения побед поражений
        //ничьий
        // Делаем запрос
        Cursor cursor = db.query(
                tableName,   // таблица
                projection,            // столбцы
                Stat.NAME_COLUMN + "=\"" + team + "\"",
                null,                  // значения для условия WHERE
                null,                  // Don't group the rows
                null,                  // Don't filter by row groups
                null);


        try {

            int colIndex = cursor.getColumnIndex(colName);

            // Проходим через все ряды
            while (cursor.moveToNext()) {
                // Используем индекс для получения строки или числа
                int current = cursor.getInt(colIndex);
                String insertQuery = "UPDATE " + tableName + " SET " + colName + "=" +
                        (current + 1) + " WHERE " + Stat.NAME_COLUMN + "=\"" + team + "\"";
                db.execSQL(insertQuery);
            }
        } finally {
            // Всегда закрываем курсор после чтения
            cursor.close();
        }
    }

    public List<List<String>> getRowsList(String colWhere, String value) throws Exception {
        List<List<String>> rowList = new ArrayList<>();

        String whereStr = (colWhere == null)? null : (colWhere + "=\"" + value + "\"");

        Cursor cursor = db.query(
                tableName,   // таблица
                projection,            // столбцы
                whereStr,                  // столбцы для условия WHERE
                null,                  // значения для условия WHERE
                null,                  // Don't group the rows
                null,                  // Don't filter by row groups
                null);

        try {

            //создаем массив индексов
            int[] idxArray = new int[projection.length];
            for (int i = 0; i < idxArray.length; i++) {
                idxArray[i] = cursor.getColumnIndex(projection[i]);
            }

            // Проходим через все ряды
            while (cursor.moveToNext()) {
                List<String> cellValueList = new ArrayList<>();
                for (int idx : idxArray
                        ) {
                    cellValueList.add(cursor.getString(idx));

                }
                rowList.add(cellValueList);
            }
        } finally {
            // Всегда закрываем курсор после чтения
            cursor.close();
        }
        return rowList;
    }

    public void insertQuery(ContentValues values) {
        db.insert(tableName, null, values);
    }

}
