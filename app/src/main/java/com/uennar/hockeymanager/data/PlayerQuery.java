package com.uennar.hockeymanager.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.uennar.hockeymanager.data.Contract.PlayerTable;

/**
 * Created by Halim on 16.09.2018.
 */

public class PlayerQuery {
    public Query query(Context context) {
        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] projection = new String[] {PlayerTable.name, PlayerTable.position, PlayerTable.team};
        Query query = new Query(db, PlayerTable.TABLE_NAME, projection);
        return query;
    }
}
