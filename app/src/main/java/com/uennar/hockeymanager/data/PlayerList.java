package com.uennar.hockeymanager.data;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Halim on 16.09.2018.
 */

public class PlayerList {

    private Context context;
    private Team team;

    public PlayerList(Context context, Team team) {
        this.context = context;
        this.team = team;
    }


    public List<Player> getPlayerList() throws Exception{
        List<Player> playersList = new ArrayList<>();
        Query query = new PlayerQuery().query(context);
        List<List<String>> rowList = query.getRowsList("team", team.getName());
        for (List<String> row: rowList
             ) {playersList.add(createPlayer(row));

        }
        return playersList;
    }
    private Player createPlayer(List<String> row)  {
        String name = row.get(0);
        String position = row.get(1);
        Player player = new Player(name, position, team);
        return player;
    }
}
