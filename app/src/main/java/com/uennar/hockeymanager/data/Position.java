package com.uennar.hockeymanager.data;

/**
 * Created by Halim on 14.09.2018.
 */

public class Position {
    public final static String forward = "fw";
    public final static String defender = "df";
}
