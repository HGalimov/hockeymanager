package com.uennar.hockeymanager.data;

import android.provider.BaseColumns;

public final class Contract {

    public Contract() {
    }



    public static final class Stat implements BaseColumns {
        public final static String TABLE_NAME = "stat";
        public final static String _ID = BaseColumns._ID;
        public final static String NAME_COLUMN = "NAME";
        public final static String WON_COLUMN = "WON";
        public final static String DRAW_COLUMN = "DRAW";
        public final static String LOST_COLUMN = "LOST";
    }

    public static final class PlayerTable implements BaseColumns {
        public final static String TABLE_NAME = "player";
        public final static String _ID = BaseColumns._ID;
        public final static String name = "name";
        public final static String position = "position";
        public final static String team = "team";
    }
}
