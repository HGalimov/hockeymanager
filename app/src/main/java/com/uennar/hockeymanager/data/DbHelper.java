package com.uennar.hockeymanager.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.uennar.hockeymanager.data.Contract.Stat;
import com.uennar.hockeymanager.data.Contract.PlayerTable;

/**
 * Created by Halim on 05.05.2018.
 */

public class DbHelper extends SQLiteOpenHelper {

    public static final String LOG_TAG = DbHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "data.db";

    private static final int DATABASE_VERSION = 1;

    private String [] teamArray;


    public DbHelper(Context context, String [] teamArray) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.teamArray = teamArray;
    }

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String intDef = " INTEGER NOT NULL DEFAULT 0 ";


        String SQL_CREATE_STAT_TABLE = "CREATE TABLE " + Stat.TABLE_NAME + " ("
                + Stat._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Stat.NAME_COLUMN + " TEXT NOT NULL, "
                + Stat.WON_COLUMN + intDef + ","
                + Stat.DRAW_COLUMN + intDef + ","
                + Stat.LOST_COLUMN + intDef + ");";


        db.execSQL(SQL_CREATE_STAT_TABLE);

        db.execSQL("CREATE TABLE " + PlayerTable.TABLE_NAME + " ("
                + PlayerTable._ID  + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + PlayerTable.name + " TEXT NOT NULL, "
                + PlayerTable.position + " TEXT NOT NULL, "
                + PlayerTable.team + " TEXT NOT NULL)");


        String SQL_INSERT_TEAMS;


        for (int i = 0; i < teamArray.length; i++) {
            SQL_INSERT_TEAMS = "INSERT INTO " + Stat.TABLE_NAME + "(" + Stat.NAME_COLUMN  + ") VALUES (\"" + teamArray[i] + "\")";

            db.execSQL(SQL_INSERT_TEAMS);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
