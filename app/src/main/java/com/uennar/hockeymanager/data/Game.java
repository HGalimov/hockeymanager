package com.uennar.hockeymanager.data;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.uennar.hockeymanager.data.Contract.Stat;

public class Game {

    private final Random random = new Random();
    private int progress;
    private String result = "";
    private int teamAScore;
    private int teamBScore;
    private Team teamA;
    private Team teamB;
    private Handler timeBarHandler;
    private Handler resultHandler;
    private Handler periodHandler;
    private DbHelper dbHelper;
    private SQLiteDatabase db;
    private Query query;
    private int period = 1;
    private Handler scoreAHandler;
    private Handler scoreBHandler;
    private Handler goalHandler;
    private String [] projection;

    public Game(Team teamA, Team teamB, Handler timeBarHandler,
                Handler resultHandler, Handler periodHandler, Handler goalHandler, Handler scoreAHandler,
                Handler scoreBHandler,
                DbHelper dbHelper) {

        this.teamA = teamA;
        this.teamB = teamB;
        this.timeBarHandler = timeBarHandler;
        this.resultHandler = resultHandler;
        this.periodHandler = periodHandler;
        this.scoreAHandler = scoreAHandler;
        this.scoreBHandler = scoreBHandler;
        this.dbHelper = dbHelper;
        this.goalHandler = goalHandler;
        db = dbHelper.getReadableDatabase();
        projection = new String[]{
                Stat.NAME_COLUMN, Stat.WON_COLUMN,
                Stat.DRAW_COLUMN, Stat.LOST_COLUMN};
        query = new Query(db, Stat.TABLE_NAME, projection);
    }

    public void play() {

        teamAScore += generateScore();
        teamBScore += generateScore();

        if (teamAScore == teamBScore) {
            result = "Draw";
            insertResult(teamA.getName(), 1);
            insertResult(teamB.getName(), 1);
        } else {
            changeScore();
        }
        if (teamAScore > teamBScore) {
            result = teamA.getName() + " won";
            insertResult(teamA.getName(), 2);
            insertResult(teamB.getName(), 0);
        }
        if (teamAScore < teamBScore) {
            result = teamB.getName() + " won";
            insertResult(teamA.getName(), 0);
            insertResult(teamB.getName(), 2);
        }

        final Map<Integer, Integer[]> teamAGoals = generateGoals(teamAScore);
        final Map<Integer, Integer[]> teamBGoals = generateGoals(teamBScore);


        teamAScore = 0;
        teamBScore = 0;

        Thread t = new Thread(new Runnable() {
            public void run() {
                while (period < 4) {
                    periodHandler.sendEmptyMessage(period);
                    Integer[] teamAGoalsInPeriod = teamAGoals.get(period);
                    Integer[] teamBGoalsInPeriod = teamBGoals.get(period);
                    while (progress < 100) {


                        progress += 10;
                        timeBarHandler.sendEmptyMessage(progress);

                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        teamAScore = kickGoal(teamAGoalsInPeriod, progress, teamAScore, scoreAHandler);
                        teamBScore = kickGoal(teamBGoalsInPeriod, progress, teamBScore, scoreBHandler);



                    }
                    progress = 0;
                    timeBarHandler.sendEmptyMessage(progress);
                    period++;
                }


                Message msg = resultHandler.obtainMessage();
                Bundle bundle = new Bundle();
                bundle.putString("result", teamAScore + "-" + result + "-" + teamBScore);
                msg.setData(bundle);
                resultHandler.sendMessage(msg);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                periodHandler.sendEmptyMessage(0);
            }
        });
        t.start();
    }
    public void play2() {

        Thread t = new Thread(new Runnable() {
            public void run() {
                while (period < 4) {
                    periodHandler.sendEmptyMessage(period);
                    while (progress < 100) {


                        progress += 20;
                        timeBarHandler.sendEmptyMessage(progress);

                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        int num = random.nextInt(2);
                        if (num == 1) {
                            num = random.nextInt(2);
                            goalHandler.sendEmptyMessage(1);
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            goalHandler.sendEmptyMessage(0);//остановить анимацию гола



                            if (num == 0) {

                                Message msg = scoreAHandler.obtainMessage();
                                Bundle bundle = new Bundle();
                                bundle.putString("score", ++teamAScore + "-" + (progress/5));
                                msg.setData(bundle);

                                scoreAHandler.sendMessage(msg);
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                scoreAHandler.sendEmptyMessage(111);

                            }
                            else {
                                Message msg = scoreBHandler.obtainMessage();
                                Bundle bundle = new Bundle();
                                bundle.putString("score", ++teamBScore + "-" + (progress/5));
                                msg.setData(bundle);

                                scoreBHandler.sendMessage(msg);
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                scoreBHandler.sendEmptyMessage(111);
                            }
                        }



                    }
                    progress = 0;
                    timeBarHandler.sendEmptyMessage(progress);
                    period++;
                }


                if (teamAScore == teamBScore) {
                    result = "Draw";
                    insertResult(teamA.getName(), 1);
                    insertResult(teamB.getName(), 1);
                }
                if (teamAScore > teamBScore) {
                    result = teamA.getName() + " won";
                    insertResult(teamA.getName(), 2);
                    insertResult(teamB.getName(), 0);
                }
                if (teamAScore < teamBScore) {
                    result = teamB.getName() + " won";
                    insertResult(teamA.getName(), 0);
                    insertResult(teamB.getName(), 2);
                }
                Message msg = resultHandler.obtainMessage();
                Bundle bundle = new Bundle();
                bundle.putString("result", teamAScore + "-" + result + "-" + teamBScore);
                msg.setData(bundle);
                resultHandler.sendMessage(msg);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                periodHandler.sendEmptyMessage(0);
            }
        });
        t.start();
    }

    private Map<Integer, Integer[]> generateGoals(int score) {
        Map<Integer, Integer[]> goals = new HashMap<>();
        for (int i = 1; i < 4; i++) {
            int countScoreForPeriod = new Random().nextInt(score + 1);
            score = score - countScoreForPeriod;
            if (i == 3 && score != 0) {
                countScoreForPeriod = score;
            }
            Integer[] goalsInPeriod = new Integer[countScoreForPeriod];//массив для голов периода
            for (int j = 0; j < goalsInPeriod.length; j++) {
                goalsInPeriod[j] = new Random().nextInt(100);
            }
            goals.put(i, goalsInPeriod);
        }
        return goals;
    }

    private int kickGoal(Integer[] goalsInPeriod, int progress, int score, Handler scoreHandler) {
        for (Integer goal : goalsInPeriod) {
            if (goal < progress && goal > progress - 10) {
                goalHandler.sendEmptyMessage(1);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                goalHandler.sendEmptyMessage(0);//остановить анимацию гола


                scoreHandler.sendEmptyMessage(++score);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                scoreHandler.sendEmptyMessage(111);

            }

        }
        return score;
    }

    private int generateScore() {
        int score = random.nextInt(11);
        return score;
    }

    private void changeScore() {
        int teamAPower = teamA.getPower();
        int teamBPower = teamB.getPower();
        int powerSum = teamAPower + teamBPower;
        int tmp = random.nextInt(powerSum);
        if (tmp < teamAPower && teamAScore < teamBScore) {
            swapScore();
            return;
        }
        if (tmp >= teamAPower && teamAScore > teamBScore) {
            swapScore();
        }
    }

    private void swapScore() {
        int tmpScore = teamAScore;
        teamAScore = teamBScore;
        teamBScore = tmpScore;
    }


    private void insertResult(String teamName, int num) {
        switch (num) {
            case 0:
                query.updStatTable(Stat.LOST_COLUMN, teamName);
                break;
            case 1:
                query.updStatTable(Stat.DRAW_COLUMN, teamName);
                break;
            case 2:
                query.updStatTable(Stat.WON_COLUMN, teamName);
                break;
        }
    }
}