package com.uennar.hockeymanager.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Halim on 21.08.2018.
 */

public class Team {

    private String name;
    private Integer imageId;
    private Integer arenaId;
    private Integer power;

    private List<Player> players;



    public Team(String name, Integer imageId, Integer arenaId, Integer power) {
        this.name = name;
        this.imageId = imageId;
        this.arenaId = arenaId;
        this.power = power;
        this.players = new ArrayList<>();
    }

    public Team(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Integer getImageId() {
        return imageId;
    }

    public Integer getArenaId() {
        return arenaId;
    }

    public Integer getPower() {
        return power;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Team team = (Team) o;

        return name.equals(team.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
