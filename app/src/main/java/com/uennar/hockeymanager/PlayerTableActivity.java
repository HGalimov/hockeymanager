package com.uennar.hockeymanager;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.uennar.hockeymanager.data.DbHelper;
import com.uennar.hockeymanager.data.PlayerQuery;
import com.uennar.hockeymanager.data.Position;
import com.uennar.hockeymanager.data.Query;
import com.uennar.hockeymanager.data.Contract.PlayerTable;

import java.util.ArrayList;
import java.util.List;


public class PlayerTableActivity extends Activity {

    private SQLiteDatabase db;
    private DbHelper dbHelper;
    private Query query;
    private String[] projection;
    private int count;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
/*
        dbHelper = new DbHelper(this);
        db = dbHelper.getReadableDatabase();
        projection = new String[] {PlayerTable.name, PlayerTable.position, PlayerTable.team};
        query = new Query(db, PlayerTable.TABLE_NAME, projection);*/
        query = new PlayerQuery().query(this);
        //addPlayerTable();
        displayPlayerTables();
    }
    private void displayPlayerTables() {

        try {
            List<String> texts = new ArrayList<>();
            texts.add("Name");
            texts.add("Position");
            texts.add("Team");
            createTableRows(texts);
            List<List<String>> rowList = query.getRowsList(null, null);
            for (List<String> cellValueList: rowList
                    ) {
                createTableRows(cellValueList);

            }

        } catch (Exception e) {
            Log.e("testException", e.getMessage());
        }


    }

    public void addPlayerTable(){
        /*ContentValues contentValues = new ContentValues();
        contentValues.put(PlayerTable.name, "Zaripov");
        contentValues.put(PlayerTable.position, Position.forward);
        contentValues.put(PlayerTable.team, "Ak Bars");
        query.insertQuery(contentValues);
        contentValues = new ContentValues();
        contentValues.put(PlayerTable.name, "Panin");
        contentValues.put(PlayerTable.position, Position.defender);
        contentValues.put(PlayerTable.team, "Salavat Yulaev");
        query.insertQuery(contentValues);*/


        /*addPlayerTables("Markov", Position.defender, "Ak Bars");
        addPlayerTables("Robinson", Position.defender, "CSKA");
        addPlayerTables("Hersley", Position.defender, "SKA");
        addPlayerTables("Bailen", Position.defender, "Traktor");
        addPlayerTables("Zabornikov", Position.defender, "Ugra");
        addPlayerTables("Kolar", Position.defender, "Amur");
        addPlayerTables("Nikulin", Position.defender, "Dinamo");*/
        /*addPlayerTables("Telegin", Position.forward, "CSKA");
        addPlayerTables("Datsyuk", Position.forward, "SKA");
        addPlayerTables("Glinkin", Position.forward, "Traktor");
        addPlayerTables("Kovalyov", Position.forward, "Ugra");
        addPlayerTables("Golubev", Position.forward, "Amur");
        addPlayerTables("Afinogenov", Position.forward, "Dinamo");
        addPlayerTables("Omark", Position.forward, "Salavat Yulaev");*/
    }

    private void addPlayerTables(String name, String position, String team) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PlayerTable.name, name);
        contentValues.put(PlayerTable.position, position);
        contentValues.put(PlayerTable.team, team);
        query.insertQuery(contentValues);
    }

    private void createTableRows(List<String> texts) {

        TableLayout tableStatLayout = findViewById(R.id.tablePlayerLayout);
        TableRow row = new TableRow(this);
        if (count == 0) {
            row.setBackgroundColor(Color.GRAY);
            count = 1;
        }

        for (String str : texts) {
            TextView textView = new TextView(this);
            textView.setText(str);
            row.addView(textView);
        }
        tableStatLayout.addView(row);
    }


    public void toMainActivity(View view) {
        Intent intent = new Intent(PlayerTableActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
